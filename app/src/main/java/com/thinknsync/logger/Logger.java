package com.thinknsync.logger;

public interface Logger {
    String tag = "logger";

    void debugLog(String tag, String msg);
    void errorLog(String tag, String msg);
    void errorLog(String tag, String msg, Throwable tr);
    boolean isDebuggable();
    void showLog(boolean showLog);
}
