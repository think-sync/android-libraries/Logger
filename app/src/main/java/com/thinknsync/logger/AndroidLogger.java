package com.thinknsync.logger;

import android.util.Log;

public class AndroidLogger implements Logger {
    private static boolean isDebug = false;
    private static AndroidLogger androidLogger = null;

    private AndroidLogger(boolean showLog){
        showLog(showLog);
    }

    public static AndroidLogger getInstance(boolean shouldShowLog){
        if(androidLogger == null){
            androidLogger = new AndroidLogger(shouldShowLog);
        }
        return androidLogger;
    }

    public static AndroidLogger getInstance(){
        return getInstance(isDebug);
    }

    @Override
    public void debugLog(String tag, String msg) {
        if (isDebug) {
            Log.d(tag, "" + msg);
        }
    }

    @Override
    public void errorLog(String tag, String msg) {
        if (isDebug) {
            Log.e(tag, "" + msg);
        }
    }

    @Override
    public void errorLog(String tag, String msg, Throwable tr) {
        if (isDebug) {
            Log.e(tag, "" + msg, tr);
        }
    }

    @Override
    public boolean isDebuggable() {
        return AndroidLogger.isDebug;
    }

    @Override
    public void showLog(boolean showLog) {
        AndroidLogger.isDebug = showLog;
    }
}
